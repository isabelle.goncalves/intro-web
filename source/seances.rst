:orphan:

================
Plan des séances
================

.. ifslides::

   .. include:: credits.rst.inc

NB: L'historique sera vu en cours magistral.

.. _seance1:

Séance 1
========

* `Introduction <web-components>`:ref:
* `Bases de HTML <html>`:doc:
  (jusqu'aux `listes <list>`:ref:, comprises)
* `Bases de CSS <css>`:doc: (jusqu'aux `couleurs <color>`:ref:, comprises)

.. _seance2:

Séance 2
========

* suite de `CSS <css>`:doc: (jusqu'aux `sélecteurs complexes<complexSelector>`:ref:, inclus)

.. _seance3:

Séance 3
========

* suite de `CSS <css>`:doc: 
   (`classes et identifiants<class>`:ref:)
* `Liens et images en HTML <link>`:ref:
* `Livre dont vous êtes le héros <donjon>`:ref:

.. _seance4:

Séance 4
========

* Suite de `Images en HTML <image>`:ref:
* `Vidéo en HTML <video>`:ref:
* `Tableau en HTML <table>`:ref:
* `Images de fond <background>`:ref:
* `Personnalisation des boîtes <boite>`:ref:

.. _seance5:

Séance 5
========

* `Pseudo-classes et pseudo-éléments <pseudos>`:ref:
* `Modes d'affichage <block>`:ref:

.. _seance6:

Séance 6
========

* `Formulaires et interactivités <form>`:ref:
* `La calculatrice interactive <exo_calculette2>`:ref:


.. _seance7:

Séance 7
========

* `Positionnement avancé <position2>`:ref: (Partie 1)

.. _seance8:

Séance 8
========

* `Positionnement avancé <position2>`:ref: (Partie 2)

.. _seance9:

Séance 9
========

* `Adaptation au media <media>`:ref:

.. _seance10:

Séance 10
=========

* Mise en production
* Métadonnées
* transitions ?

.. _seance11:

Séances 11 à 13
===============

* Projet
